"""
This script copies the last item matching some regular expression from
one history item to another.
"""
from scripts.db_shell import *
import re

def find_history(name):
    query = sa_session.query( History ).filter_by( name=name )
    if query.count() == 0:
        raise Exception
    else:
        return query.first()

def get_history_items(name):
    history = find_history(name)
    hdas = []
    query = sa_session.query( HistoryDatasetAssociation ) \
            .filter( HistoryDatasetAssociation.history == history ) \
            .options( eagerload( "children" ) ) \
            .join( "dataset" ) \
            .options( eagerload_all( "dataset.actions" ) ) \
            .order_by( HistoryDatasetAssociation.hid )
    for hda in query:
        hdas.append(hda)
    return hdas
    
def get_data(dataset_id):
    data = sa_session.query( HistoryDatasetAssociation ).get( dataset_id )
    return data

def copy_dataset_from_item( dataset, history ):
    """ Helper method for copying datasets. """
    dataset_copy = dataset.copy( copy_children = True )
    history.add_dataset( dataset_copy, set_hid=True )
    sa_session.flush()
    

def find_last_matching( items, regex ):
    last_matching = None
    for item in items:
        if re.search(regex, item.name):
            last_matching = item
    if not last_matching:
        raise Exception
    return last_matching

def copy_last_item_to_history_with_name( from_history_name, to_history_name, regex=None):
    if not regex:
        regex=".*"
    from_history_items = get_history_items(from_history_name)
    to_history=find_history(to_history_name)
    history_item=find_last_matching(from_history_items, regex)
    copy_dataset_from_item(history_item, to_history)

from optparse import OptionParser
parser=OptionParser()
parser.add_option("--from_history")
parser.add_option("--to_history")
parser.add_option("--matching")

(options, args) = parser.parse_args()
copy_last_item_to_history_with_name(options.from_history, options.to_history, options.matching)
