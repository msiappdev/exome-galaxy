"""
This script produces a regular expression for use with find that will
find the path to the last item matching some regular expression in a
history.
"""
from scripts.db_shell import *
import re
from ConfigParser import ConfigParser
import inspect

def find_history(name):
    query = sa_session.query( History ).filter_by( name=name )
    if query.count() == 0:
        raise Exception
    else:
        return query.first()

def get_history_items(name):
    history = find_history(name)
    hdas = []
    query = sa_session.query( HistoryDatasetAssociation ) \
            .filter( HistoryDatasetAssociation.history == history ) \
            .options( eagerload( "children" ) ) \
            .join( "dataset" ) \
            .options( eagerload_all( "dataset.actions" ) ) \
            .order_by( HistoryDatasetAssociation.hid )
    for hda in query:
        hdas.append(hda)
    return hdas
    
def find_last_matching( items, regex ):
    last_matching = None
    for item in items:
        if re.search(regex, item.name):
            last_matching = item
    if not last_matching:
        raise Exception
    return last_matching

def parse_options(name="options"):
    config = ConfigParser()
    file_path = os.path.dirname(inspect.getfile(inspect.currentframe()))
    options_path = os.path.join(file_path, "universe.ini")
    config.readfp(open(options_path))
    return config.get("", "")


def build_find_pattern(history_name, regex):
    if not regex:
        regex = ".*"
    items = get_history_items(history_name)
    matching = find_last_matching(items, regex)
    file_path = os.path.dirname(inspect.getfile(inspect.currentframe()))
    database_files_path = os.path.join(file_path, "database", "files")
    dataset_id = matching.dataset.id
    dataset_name = matching.name.replace(" ", "_")
    print "./%s/*/dataset_%d.dat %s" % ( database_files_path, dataset_id, dataset_name )

from optparse import OptionParser
parser=OptionParser()
parser.add_option("--history_name")
parser.add_option("--matching")

(options, args) = parser.parse_args()
build_find_pattern(options.history_name, options.matching)

