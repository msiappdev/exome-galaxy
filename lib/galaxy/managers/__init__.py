""" 'Business logic' independent of web transactions/user context (trans)
should be pushed into models - but logic that requires the context trans 
should be placed under this module.
"""
