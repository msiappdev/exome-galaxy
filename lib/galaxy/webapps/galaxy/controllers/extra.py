"""
"""
from galaxy.web.framework.helpers import time_ago, grids
from galaxy.web.base.controller import *
from galaxy.model.orm import *
from galaxy import util, model
import logging, os, string, re, socket, glob
from random import choice
import subprocess
from galaxy.web.form_builder import * 
from galaxy.util.json import from_json_string, to_json_string
from galaxy.web.framework.helpers import iff
from galaxy.security.validate_user_input import validate_email, validate_publicname, validate_password

log = logging.getLogger( __name__ )

class Extra( BaseUIController, UsesFormDefinitionsMixin ):

    def __execute( self, cmd ):
        proc = subprocess.Popen( args=cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, close_fds=True )
        stdout, stderr = proc.communicate()
        return_code = proc.returncode
        if return_code == 0:
            return """%s""" % stdout
        else:
            return """Error completing function: stdout was <%s>, stderr was <%s> and return code was %d""" % (stdout, stderr, return_code)

#    @web.expose
#    def shutdown( self, trans, cntrller, webapp='galaxy', **kwd ):
#        galaxy_root = trans.app.config.root
#        return self.__execute( os.path.join( galaxy_root, "scripts", "clia_shutdown.sh" ) )

#    @web.expose
#    def release( self, trans, cntrller, webapp='galaxy', **kwd ):
#        galaxy_root = trans.app.config.root
#        return self.__execute( os.path.join( galaxy_root, "scripts", "clia_release.sh" ) )
