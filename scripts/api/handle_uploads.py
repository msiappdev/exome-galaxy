""" Create a data library for data uploaded to /home/galaxy/uploaded_data. Simply call this script with an admin API key.
"""
import os
import shutil
import sys
import time
from optparse import OptionParser
sys.path.insert( 0, os.path.dirname( __file__ ) )
from common import submit, display

def main(api_key, db_key='', api_url="http://localhost", in_folder = "/mnt/uploaded_data", data_library="Uploaded Data", history_api_key=None, history_name=None):
    # Find/Create data library with the above name.  Assume we're putting datasets in the root folder '/'
    matching_history = None
    if history_name and history_api_key:
        histories = display(history_api_key, api_url + "/api/histories", return_formatted=False)
        for history in histories:
            if history['name'] == history_name:
                matching_history = history
                print matching_history['url']
                print display(history_api_key, "http://localhost%s" % matching_history['url'])
    libs = display(api_key, api_url + '/api/libraries', return_formatted=False)
    library_id = None
    for library in libs:
        if library['name'] == data_library:
            library_id = library['id']
    if not library_id:
        lib_create_data = {'name':data_library}
        library = submit(api_key, api_url + '/api/libraries', lib_create_data, return_formatted=False)
        print "Created library: ", library
        library_id = library['id']
    folders = display(api_key, api_url + "/api/libraries/%s/contents" % library_id, return_formatted = False)
    for f in folders:
        if f['name'] == "/":
            library_folder_id = f['id']
    if not library_id or not library_folder_id:
        print "Failure to configure library destination."
        sys.exit(1)
    library_datasets = []
    for fname in os.listdir(in_folder):
        if not "_part" in fname: 
            print fname
            fullpath = os.path.join(in_folder, fname)
            if os.path.isfile(fullpath):
                data = {}
                data['folder_id'] = library_folder_id
                data['file_type'] = 'auto'
                data['dbkey'] = db_key
                data['upload_option'] = 'upload_paths'
                data['filesystem_paths'] = fullpath
                data['link_data_only'] = 'link_to_files'
                data['create_type'] = 'file'
                print "api_key: ", api_key, " api_url: ", api_url, " library_id: ", library_id, " data: ", data
                libset = submit(api_key, api_url + "/api/libraries/%s/contents" % library_id, data, return_formatted = False)
                library_datasets.append(libset)
        else: 
            print "Skipping part file: %s" % (fname)
    # Give time for library datasets to have types defined.
    time.sleep(15)
    for libset in library_datasets:
        if matching_history:
            history_url = "%s%s/contents"  % (api_url, matching_history["url"])
            data = {}
            data['content'] = libset[0]['id']
            data['source'] = 'library'
            history_addition = submit(history_api_key, history_url, data)
                       
if __name__ == '__main__':
    api_key = sys.argv[1]
    parser = OptionParser('')
    parser.add_option('--api_key', default=None)
    parser.add_option('--db_key', default='')
    parser.add_option('--history', default=None)
    parser.add_option('--history_api_key', default=None)
    (options, args) = parser.parse_args()
    api_key = options.api_key
    main(api_key, db_key=options.db_key, history_name=options.history, history_api_key=options.history_api_key)
