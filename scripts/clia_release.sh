#!/bin/bash

current_directory=$(readlink -f `dirname $0`)
galaxy_dir=$current_directory/..
cd $galaxy_dir

source runtime_properties

FAIRVIEW_EMAIL=trevor@umn.edu
FROM_HISTORY=""
TO_HISTORY=""
MATCHING=".*"

## Copy data from one history to the other
echo "<p>Copying data.</p>"
python copy.py --from_history="$FROM_HISTORY" --to_history="$TO_HISTORY"  --matching="$MATCHING" > ~/release_ouptut.txt
if [ ! "$?" == "0" ];
then
    echo "Problem coping data, data not released."
    exit 1
fi

echo "<p>Sending e-mail to %FAIRVIEW_EMAIL</p>"
echo "To: $FAIRVIEW_EMAIL" > /tmp/mail
echo "Subject: BMGC Data Release" >> /tmp/mail
echo "" >> /tmp/mail
echo "Data from the Unviersity of Minnesota BMGC is ready for you at http://$FQDN/ in a history named $TO_HISTORY. Remember to shutdown the Galaxy instance when you are done with it." >> /tmp/mail
/usr/sbin/ssmtp $FAIRVIEW_EMAIL < /tmp/mail
if [ ! "$?" == "0" ];
then
    echo "Problem sending e-mail."
    exit 1
fi
echo "<p>E-mail sent</p>"
