#!/bin/bash

current_directory=$(readlink -f `dirname $0`)
galaxy_dir=$current_directory/..
cd $galaxy_dir

# Loads DESTINATION_DIR
source runtime_properties

FAIRVIEW_HISTORY_NAME="moo"
REMOTE_USER="evan"
REMOTE_HOST="login.msi.umn.edu"
DESTINATION_DIR="."

for pattern in ".*";
do
    path_and_name=$(bash -c "python file_path.py --history_name=\"$FAIRVIEW_HISTORY_NAME\" --matching=\"$pattern\" | tail -n 1 ")
    if [ "$?" != "0" ];
    then
        echo "<p>Failed to find file(s) to upload.</p>"
        exit 1
    fi
    set -- $path_and_name
    path_pattern=$1
    file_name=$2
    path=$(bash -c "find -L . -path \"$path_pattern\"")
    scp -o 'StrictHostKeyChecking=no' $path $REMOTE_USER@$REMOTE_HOST:$DESTINATION_DIR/$file_name
    if [ "$?" != "0" ];
    then
        echo "<p>File(s) to upload found, but upload failed.</p>"
        exit 1
    fi
done

echo "<p>Galaxy is now shutting down!</p>"
# Cancel queued shutdown so we can just halt now.
#sudo shutdown -c
sudo halt -f 

