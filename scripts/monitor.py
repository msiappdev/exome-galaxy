import os 
import sys
import traceback

new_path = [ os.path.join( os.getcwd(), "lib" ) ]
new_path.extend( sys.path[1:] )  # remove scripts/ from the path
sys.path = new_path

from galaxy import eggs
eggs.require('boto')
eggs.require("bioblend >= 0.5.1")


import string
#import socket

import re
import time
import logging
import signal

from argparse import ArgumentParser

from bioblend import galaxy
#from bioblend.galaxy.objects import *

from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from smtplib import SMTP 



class Timeout(): 

    """Timeout class using ALARM signal."""
    class Timeout(Exception):
        pass
 
    def __init__(self, sec):
        self.sec = sec
 
    def __enter__(self):
        signal.signal(signal.SIGALRM, self.raise_timeout)
        signal.alarm(self.sec)
 
    def __exit__(self, *args):
        signal.alarm(0)    # disable alarm
 
    def raise_timeout(self, *args):
        raise Timeout.Timeout()


class GalaxyWorkflowRunner(): 


    def __init__(self, remote_host, remote_user, remote_apikey, jobid, project_name, delete_histories=False, log_level=logging.DEBUG):
        logger = logging.getLogger()
        logger.setLevel(log_level)

        logging.getLogger('requests.packages.urllib3.connectionpool').setLevel(logging.ERROR)
        logging.getLogger('bioblend').setLevel(logging.WARN)

        ch = logging.StreamHandler()
        ch.setLevel(log_level)
        formatter = logging.Formatter('[ %(asctime)s ] - %(levelname)s - %(message)s')
        ch.setFormatter(formatter)
        logger.addHandler(ch)
        self.logger = logger
	
        self.email_frm = os.getenv('EMAIL_FROM', 'CLIA INSTANCE %s <devnull@msi.umn.edu>' % (remote_host))

        self.remote_host = remote_host
        self.remote_user = remote_user
        self.remote_apikey = remote_apikey

        self.polling_interval = 60 # seconds
        self.submit_timeout = 10 # seconds
        self.poll_timeout = 60 # seconds
        self.break_on_error = True

        self.w_name = "Not Set. Call run_workflow() first."
        self.h_name = "Not Set. Call run_workflow() first."
        self.l_name = "Not Set. Call run_workflow() first."

        self.jobid = jobid
        self.sample_name = project_name

        self.delete_histories = delete_histories

        #self.gi = GalaxyInstance(url=remote_host, api_key=remote_apikey)
        self.gi = galaxy.GalaxyInstance(url=remote_host, key=remote_apikey)

    
    def _send_email_alert(self, alert, alert_type='Status', is_error=False, additional_instructions=""): 
        logger = self.logger
        try: 
            if is_error: 
                alert_type = 'Error'
                additional_instructions = """Please resolve the issue and manually finish the workflow. When complete, remember to shutdown the instance."""
            fromaddr = self.email_frm
            toaddr = self.remote_user
            msg = MIMEMultipart()
            msg['From'] = fromaddr
            msg['To'] = toaddr
            msg['Subject'] = "(%s) %s (Sample: %s, VM: %s)" % (alert_type, self.w_name, self.sample_name, self.remote_host)
            body = """
Instance:\thttp://%s
History:\t%s
Workflow:\t%s
Sample Name:\t%s
PBS JobId:\t%s

######### %s Message #########

%s

%s""" % (self.remote_host, self.h_name, self.w_name, self.sample_name, self.jobid, alert_type, alert, additional_instructions)
            msg.attach(MIMEText(body, 'plain'))

            server = SMTP('mail.msi.umn.edu')
            server.ehlo()
            text = msg.as_string()
            server.sendmail(fromaddr, toaddr, text)
            server.close()
        except StandardError as e: 
            logger.error(e)
        except Exception as e:
            logger.error(e)


    def _delete_histories(self, h_name):
        #########
        # Delete pre-existing histories with the specified h_name
        #########
        logger = self.logger
        logger.info("Deleting Histories...") 
        if 1:
            histObjs = [w for w in self.gi.histories.get_histories() if w['name'] == h_name]
            for h in histObjs:
                logger.info("Deleting existing history: %s" % h['id'])
                try: 
                    # NOTE: purging is disabled on these instances (purge=True is not allowed)
                    status = self.gi.histories.delete_history(h['id'])
                except Exception as e:
                    logger.error("Failed to delete a workflow. Ignoring failure...")
                    logger.error("----------")
                    logger.error(e)
                    logger.error("----------")

        logger.info("########")


    def get_uploaded_datasets(self, l_name):
        #########
        # Get datasets uploaded to l_name
        #########
        logger = self.logger
        logger.info("Getting Uploaded Datasets...") 
        lib = self._get_library(l_name)
        logger.info("Library ID: %s" % lib['id'])
        datasets = self.gi.libraries.show_library(lib['id'], contents=True)
        logger.info("########")
        return datasets

    def _get_library(self, l_name):
        library = [w for w in self.gi.libraries.get_libraries() if w['name'] == l_name][0]
        return library

    def _get_workflow(self, w_name):
        logger = self.logger
        workflow = [w for w in self.gi.workflows.get_workflows() if w['name'] == w_name]
        if workflow: 
            return workflow[0]
        else: 
            raise Exception('No workflows matching: %s\nOptions are: %s' % (w_name, [w['name'] for w in self.gi.workflows.get_workflows()]))

    def get_workflow_inputs(self, w_name): 
        #########
        # Get the inputs required to run the workflow
        #########
        logger = self.logger
        logger.info("Getting Workflow Inputs...") 
        workflow = self._get_workflow(w_name)
        inputs = self.gi.workflows.show_workflow(workflow['id'])['inputs']
        #inputs = [inputs[i] for i in inputs]
        logger.info("Input files: %s" % (inputs))
        logger.info("########")
        return inputs

    def map_datasets_to_inputs(self, datasets, inputs):
        #########
        # Map datasets to inputs based on input label (regexps)
        #########
        logger = self.logger
        logger.info("Mapping Datasets to Inputs...") 
        dataset_map = {}
        dataset_names = [d['name'] for d in datasets]
        unmatched_list = []
        for i in inputs:
            iname = inputs[i]['label']
            found = False
            reobj = re.compile('.*' + iname)
            for d in datasets:
                if reobj.match(d['name']):
                    logger.info("Matched Input: %s <= %s [ \'%s\' <= \'%s\' ]" % (i,d['id'],iname, d['name']))
                    dataset_map[i] = {'id': d['id'], 'src': 'ld'}
                    found = True
            if not found:
                logger.warn("----- No Match: %s -----" % (iname) )
                unmatched_list.append(iname)
        if unmatched_list:
            e = "Could not find workflow input files matching:\n\t%s\n on http://%s.\n\nAvailable Inputs:\n\t%s\nAre you running the right workflow?" % (",\n\t".join(unmatched_list), self.remote_host, ",\n\t".join(dataset_names))
            raise Exception(e)
        logger.info("########")
        return dataset_map

    def _get_history_datasets(self, h_id): 
        # TODO: Bioblend requries list() to refresh the history. perhaps there is a shortcut? 
        #hist = [h for h in self.gi.histories.get_histories() if h['name'] == h_name][0]
        datasets = self.gi.histories.show_history(h_id, contents=True)
        return datasets

    def monitor_jobs(self, hist): 
        logger = self.logger
    	_PENDING_DS_STATES = set(
    	["new", "upload", "queued", "running", "setting_metadata"]
    	)

    	datasets = self._get_history_datasets(hist['id'])
    	total_jobs = len(datasets)

    	#--
    	def _get_error_info(hda):
            logger = self.logger
            msg = hda['name']
            try:
                msg += ' (%s): ' % hda['id']
                msg += hda['state']
                msg += ", %s" % (hda['misc_info'])
            except StandardError as e:  # avoid 'error while generating an error report'
                msg += ': error generating report\n'
                msg += '%s' % (e)
            return msg
        #--
        def poll(ds_list):
            pending = []
            for ds in ds_list:
                ds = self.gi.datasets.show_dataset(ds['id'])
                logger.info('%s (%s): %s' % (ds['id'], ds['name'], ds['state']))
                if self.break_on_error and (ds['state'] == 'error' or ds['state'] == 'discarded'):
                    # Disable failure alert after the first error, and just let the workflow finish
                    self.break_on_error = False 
                    self.workflow_errors = True
                    raise RuntimeError(_get_error_info(ds))
                if ds['state'] in _PENDING_DS_STATES:
                    pending.append(ds)
            return pending
        #--
        while datasets:
            try: 
                with Timeout(self.poll_timeout):
                    new_datasets = self._get_history_datasets(hist['id'])
                if total_jobs < len(new_datasets): 
                    logger.warn("++++ Workflow Expanded %d Steps. Monitoring New Datasets ++++" % (len(new_datasets) - len(datasets)))
                    total_jobs = len(new_datasets)
                    datasets = new_datasets
                datasets = poll(datasets)
                logger.info("Total Steps = %d; Queued/Running Steps = %d" % (total_jobs, len(datasets)))
                percent_complete = 100 * (total_jobs - len(datasets)) / total_jobs
                logger.info("------------ %d%% complete ------------" % (percent_complete))
                if percent_complete < 100: time.sleep(self.polling_interval) 
            except Timeout.Timeout: 
                logger.warn("-----> Poll timed out. Trying again...")
            except ValueError as e:  # NOTE: includes simplejson.decoder.JSONDecodeError
                logger.warn("-----> Poll returned bad JSON object. Trying again... \n%s\n" % e)
            except RuntimeError as e: 
                logger.error("\n\n%s\n" % e)
                raise e
                #self._send_email_alert(e, is_error=True)


    def run_workflow(self, w_name, h_name, l_name, silent=False): 
        #########
        # Execute Workflow on inputs 
        # (NOTE: this should be asynchronous, but Galaxy does not let go of our
        # connection immediately, which makes it appear synchronous)
        # NOTE: uses signal.alarm to interrupt after 10 minutes and continue watching. 
        # http://pythonadventures.wordpress.com/2012/12/08/raise-a-timeout-exception-after-x-seconds/
        #########
        logger = self.logger
        logger.info("Running workflow...") 

        self.h_name = h_name
        self.w_name = w_name
        self.l_name = l_name 

        self.workflow_errors = False

        logger.warn('Sleeping 15 seconds to ensure nohup is successful')
        time.sleep(15)

        try: 
            if self.delete_histories == True: 
                self._delete_histories(h_name)

            inputs = self.get_workflow_inputs(w_name)
            input_datasets = self.get_uploaded_datasets(l_name)
            dataset_map = self.map_datasets_to_inputs(input_datasets, inputs)

            if not silent:
                self._send_email_alert("Workflow \'%s\' Started on http://%s" % (w_name, self.remote_host), is_error=False)
            try: 
                with Timeout(self.submit_timeout):
                    # TODO: mapq should have a default value in the workflow, and be exposed to user submit via input arguments
                    workflow = self._get_workflow(w_name)
                with Timeout(self.submit_timeout):
                    output = self.gi.workflows.run_workflow(workflow['id'], dataset_map=dataset_map, params=None, history_name=h_name, import_inputs_to_history=True, replacement_params=None)
                with Timeout(self.submit_timeout):
                    hist = [h for h in self.gi.histories.get_histories() if h['id'] == output['history']][0]
            except IndexError as e: 
                logger.warn("Workflow submitted, but Galaxy did not return the History.") 
                logger.warn("Traceback: %s" % (traceback.format_exc()))
                raise e
            except Timeout.Timeout:
                # TODO: handle the case where delete_histories is False and the
                # workflow submit times out. We need to distinguish the newest
                # history from the rest. 
                logger.warn("Workflow submit timed out. Disconnecting and continuing to monitor progress...")
                hist_attempts = 0
                max_hist_attempts = 10
                while hist_attempts < max_hist_attempts: 
                    try: 
                        with Timeout(self.submit_timeout):
                            logger.info("Getting histories...")
                            hist = [h for h in self.gi.histories.get_histories() if h['name'] == h_name][0]
                            hist_attempts = max_hist_attempts
                    except:
                        logger.warn("Traceback: %s" % (traceback.format_exc()))
                        logger.warn("Workflow submitted, but Galaxy did not return the History. Retrying get_histories()...")
                        if hist_attempts < max_hist_attempts: 
                            logger.warn("Sleeping...")
                            time.sleep(self.submit_timeout)
                        else:
                            raise 
                    finally:
                        hist_attempts = hist_attempts + 1

            logger.info("Workflow %s executing in history %s (%s)" % (w_name, h_name, hist['id']))

            logger.info("------------ Execution Started ------------")
            self.monitor_jobs(hist)

        except RuntimeError as e: 
            self._send_email_alert(e, is_error=True)
            # CONTINUE MONITORING UNTIL 100% COMPLETE
            #self.monitor_jobs(hist)
        except Exception as e: 
            tb = traceback.format_exc()
            logger.error(e)
            logger.error(tb)
            # TODO: advanced logic in the event of a failure. 
            # - If dataset could not be found (with regular expressions), email launcher
            # - etc.
            self._send_email_alert(e, is_error=True)
            raise e
        except StandardError as e: 
            tb = traceback.format_exc()
            logger.error(e)
            logger.error(tb)
            self._send_email_alert(e, is_error=True)
            raise e
        except:
            tb = traceback.format_exc()
            logger.error(tb)
            self._send_email_alert('Unknown exception/error', is_error=True)
            raise 

        if not self.workflow_errors: 
            if not silent:
                self._send_email_alert("Workflow \'%s\' finished completely on http://%s" % (w_name, self.remote_host), is_error=False)
                #TODO: shutdown instance here.
                shutdown_attempts = 0
                while shutdown_attempts < 5: 
                    try: 
                        self.run_workflow("CLIA_workflow_complete.ga", h_name, l_name, silent=True)
                    except: 
                        logger.error("Problems running shutdown...retrying")
                    finally:
                        shutdown_attempts = shutdown_attempts + 1
                self._send_email_alert("Instance http://%s could not be shutdown automatically!" % (self.remote_host), is_error=True)
        #else: 
            #if not silent:
            #    self._send_email_alert("Workflow \'%s\' finished with errors on http://%s" % (w_name, self.remote_host), is_error=False, additional_instructions="*** THIS INSTANCE NEEDS TO BE MANUALLY SHUT DOWN***")
         
    def __del__(self):
        self.logger.info("######## END")



if __name__ == "__main__":

    parser = ArgumentParser("Connects to an on-demand Galaxy instance and launches a workflow on it.")
    parser.add_argument('--remote_host', default=[], required=True)
    parser.add_argument('--workflow', nargs='+', default=[], required=True)
    parser.add_argument('--galaxy_user', default=[])
    parser.add_argument('--galaxy_apikey', default=[])
    parser.add_argument('--pbs_jobid', default='XXXXX')
    parser.add_argument('--project_name', default='XXXXX')
    args = parser.parse_args()

    w_name = " ".join(args.workflow)
    h_name = w_name
    l_name = "Uploaded Data"

    # TODO: if an alert was issued, continue to monitor instance until user
    # logs in and shuts down the instance. 
    m = GalaxyWorkflowRunner(args.remote_host, args.galaxy_user, args.galaxy_apikey, args.pbs_jobid, args.project_name)
    m.run_workflow(w_name, h_name, l_name)

