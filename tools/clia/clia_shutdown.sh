#!/bin/bash

current_directory=$(readlink -f `dirname $0`)
galaxy_dir=$current_directory/..
cd $galaxy_dir

# Loads DESTINATION_DIR, REMOTE_USER, REMOTE_HOST, FQDN
source /opt/galaxy/web/runtime_properties

#e-mail user
#ssh -q -o 'StrictHostKeyChecking=no' $REMOTE_USER@login.msi.umn.edu "echo '$1 has initiated the immediate shutdown of the machine for $PROJECT_NAME on http://$FQDN.  $2' | mail -s 'Shutting down $PROJECT_NAME' $1 $REMOTE_USER@msi.umn.edu"

ssh -q -o 'StrictHostKeyChecking=no' $REMOTE_USER@login.msi.umn.edu "echo '$1 has initiated the immediate shutdown of the machine for $PROJECT_NAME on http://$FQDN.  $2' | mail -s 'Shutting down http://$FQDN ($PROJECT_NAME)' trevor@msi.umn.edu"

#echo "Galaxy is now shutting down!"

if [ $USE_NOVA ]; then
# OpenStack
    nova --bypass-url $OS_BYPASS_URL delete $( nova --bypass-url $OS_BYPASS_URL list | grep $FQDN | awk '{print $2}' )
else
# Cancel queued shutdown so we can just halt now.
# AWS EC2
    #sudo shutdown -c
    sudo halt -f
fi

sleep 60

ssh -q -o 'StrictHostKeyChecking=no' $REMOTE_USER@login.msi.umn.edu "echo '$1 failed the immediate shutdown of the machine for $PROJECT_NAME on http://$FQDN.  $2' | mail -s 'Shutdown failure $PROJECT_NAME' trevor@msi.umn.edu"
