#!/bin/bash

current_directory=$(readlink -f `dirname $0`)
galaxy_dir=$current_directory/..
cd $galaxy_dir

# Loads DESTINATION_DIR, REMOTE_USER, REMOTE_HOST, PROJECT_NAME
source /opt/galaxy/web/runtime_properties

message="Completed workflow execution"

email_addr=$1

# shift: pop off first argument and shift remaining left.
shift
while (( "$#" ));
do
    #file_name=$( basename $1 )
    path=$( readlink -f $1 )
    echo "Downloading $path to $REMOTE_USER@$REMOTE_HOST:$DESTINATION_DIR/${PROJECT_NAME}__$2"
    scp -q -o 'StrictHostKeyChecking=no' $path $REMOTE_USER@$REMOTE_HOST:$DESTINATION_DIR/${PROJECT_NAME}__$2
    ssh -q -o 'StrictHostKeyChecking=no' $REMOTE_USER@$REMOTE_HOST "chmod 770 $DESTINATION_DIR/${PROJECT_NAME}__$2"
    if [ "$?" != "0" ];
    then
        echo "  Downlaod failed"
    fi

    # pop off two args and move remaining left
    shift
    shift
done

ssh -q -o 'StrictHostKeyChecking=no' $REMOTE_USER@$REMOTE_HOST "echo $message | mail -s 'Workflow Complete for $PROJECT_NAME' $email_addr"
#echo $message
