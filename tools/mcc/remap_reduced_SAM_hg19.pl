#!/usr/bin/perl -w
#
# Usage: remap_reduced_SAM_hg19.pl <SAM-file>
#
# Description:
#   This simple script converts a SAM file that was created by mapping
# to a reduced reference genome back to its original coordinates.
#
# For example, you could just pull out select regions of chromosome 1
# to map to, e.g.,
#
# @SQ     SN:hg19_canonical_chr1_954503_958842_+  LN:4339
# @SQ     SN:hg19_canonical_chr1_968486_971704_+  LN:3218
# @SQ     SN:hg19_canonical_chr1_975034_992496_+  LN:17462
#
# then you would get mappings like the following:
#
# HWUSI-EAS1737:7:99:16711:4663#CAGATC    99      hg19_canonical_chr1_954503_958842_+     3084    29      76M     =       3398    388     GTCTGGCGGTACTTGAAGGGCAAAGACCTGGTGGCCCGGGAGAGCCTGCTGGACGGCGGCAACAAGGTGGTGATCA    HEHGBHHHHHBGGGGBD<<><FFDDEBEC8D??BBFBEE<<GDGGGGDGF@GGDD>GDG8G@DGB?@:@?>C8AC2    RG:Z:index7_sample1XT:A:U  NM:i:0  SM:i:29 AM:i:29 X0:i:1  X1:i:0  XM:i:0  XO:i:0  XG:i:0  MD:Z:76
# HWUSI-EAS1737:7:99:16711:4663#CAGATC    147     hg19_canonical_chr1_954503_958842_+     3398    29      74M2S   =       3084    -388    ACAGGTTGCAGGGGTCGCTGTGCGGGGTCCTTTCGTGCTGTGCCGGAGGCTGCAGCACACGGTGTCTTGTAGTCTG    =5:=:;EECCC7A;A2@:984@CF?>AB<D8D@E=@FEEEE8DDEG?>BBB?4BEE>B>@:GB=BGGGEGGG>GB?    RG:Z:index7_sample1XT:A:M  NM:i:1  SM:i:29 AM:i:29 XM:i:1  XO:i:0  XG:i:0  MD:Z:70G3
#
# instead of the mappings you would have obtained against the full reference:
#
# HWUSI-EAS1737:7:99:16711:4663#CAGATC    99      chr1     957587    29      76M     =       957901    388     GTCTGGCGGTACTTGAAGGGCAAAGACCTGGTGGCCCGGGAGAGCCTGCTGGACGGCGGCAACAAGGTGGTGATCA    HEHGBHHHHHBGGGGBD<<><FFDDEBEC8D??BBFBEE<<GDGGGGDGF@GGDD>GDG8G@DGB?@:@?>C8AC2    RG:Z:index7_sample1XT:A:U  NM:i:0  SM:i:29 AM:i:29 X0:i:1  X1:i:0  XM:i:0  XO:i:0  XG:i:0  MD:Z:76
# HWUSI-EAS1737:7:99:16711:4663#CAGATC    147     chr1     957901    29      74M2S   =       957587    -388    ACAGGTTGCAGGGGTCGCTGTGCGGGGTCCTTTCGTGCTGTGCCGGAGGCTGCAGCACACGGTGTCTTGTAGTCTG    =5:=:;EECCC7A;A2@:984@CF?>AB<D8D@E=@FEEEE8DDEG?>BBB?4BEE>B>@:GB=BGGGEGGG>GB?    RG:Z:index7_sample1XT:A:M  NM:i:1  SM:i:29 AM:i:29 XM:i:1  XO:i:0  XG:i:0  MD:Z:70G3
#
# WARNING: Currently this script only works for hg19_canonical
#
use strict;
use Getopt::Long;

my $log;
my $log_h;
my %options = (
    "log=s"    => \$log
    );

GetOptions(%options);
if (defined($log)) {
    open($log_h, ">", $log) || die "Unable to write to $log: $!\n";
} else {
    open($log_h, ">", \*STDERR) || die "Unable to write to STDERR: $!\n";
}
my $base_genome = 'hg19_canonical';
my $oligo_control_genome = 'Arabidopsis_thaliana_TAIR9';

# 1. First print out new header
print <<"END_HG19_HEADER";
\@SQ	SN:chrM	LN:16571
\@SQ	SN:chr1	LN:249250621
\@SQ	SN:chr2	LN:243199373
\@SQ	SN:chr3	LN:198022430
\@SQ	SN:chr4	LN:191154276
\@SQ	SN:chr5	LN:180915260
\@SQ	SN:chr6	LN:171115067
\@SQ	SN:chr7	LN:159138663
\@SQ	SN:chr8	LN:146364022
\@SQ	SN:chr9	LN:141213431
\@SQ	SN:chr10	LN:135534747
\@SQ	SN:chr11	LN:135006516
\@SQ	SN:chr12	LN:133851895
\@SQ	SN:chr13	LN:115169878
\@SQ	SN:chr14	LN:107349540
\@SQ	SN:chr15	LN:102531392
\@SQ	SN:chr16	LN:90354753
\@SQ	SN:chr17	LN:81195210
\@SQ	SN:chr18	LN:78077248
\@SQ	SN:chr19	LN:59128983
\@SQ	SN:chr20	LN:63025520
\@SQ	SN:chr21	LN:48129895
\@SQ	SN:chr22	LN:51304566
\@SQ	SN:chrX	LN:155270560
\@SQ	SN:chrY	LN:59373566
END_HG19_HEADER

# 2. Now read in the file replacing text as needed
while (<>) {
    if (/^\@SQ/) { next; }
    elsif (/^\@/) { print; } # read group declaration or other header
    else {
	my @fields = split /\t/;
	my ($chr, $start_of_block) = ('', '');
	if ($fields[2] =~ /^$base_genome\_(\w+)\_(\d+)\_\d+\_\+/) {
	    ($chr, $start_of_block) = ($1, $2);
	    $fields[2] = $chr;
	}
	elsif ($fields[2] eq '*') { print; next;} # unmapped
	elsif ($fields[2] =~ /^$oligo_control_genome\_(\S+)/) {
	    print $log_h "Encountered $oligo_control_genome Control oligo $1.\n";
	    next;
	}
	else {
	    die "ERROR: expected targets of the form $base_genome" . "_chr_startpos_endpos_+.\nEncountered $fields[2]. Please re-create your reduced genome correctly and map again.\n$_";
	}

	$fields[3] += $start_of_block;

        if ($fields[6] =~ /^\=/) { # mate on same chromosome
	    $fields[7] += $start_of_block;
        }
	elsif ($fields[6] =~ /^$base_genome\_(\w+)\_(\d+)\_\d+\_\+/) {
	    ($chr, $start_of_block) = ($1, $2);
	    $fields[6] = $chr;
	    $fields[7] += $start_of_block;
	}

	my $full_line = join("\t", @fields);

	# Now deal with multiple mappings signified by XA: code
	if ($full_line =~ /\s(XA:\w+:)(\S+)/) {
	    my ($new_XA_str, $old_xref) = ($1, $2);
	    my @xref_str = split /\;/, $old_xref;

	    foreach my $xref_entry (@xref_str) {
		my ($target, $pos, $cigar, $dist) = split /,/, $xref_entry;
		if ($target =~ /^$base_genome\_(\w+)\_(\d+)\_\d+\_\+/) {
		    ($chr, $start_of_block) = ($1, $2);
		    $target = $chr;
		    $pos -= $start_of_block;
		    my $new_entry = join (",", $target, $pos, $cigar, $dist);
		    if ($new_XA_str =~ /:$/) {
			$new_XA_str .= $new_entry;
		    }
		    else {
			$new_XA_str .= ";$new_entry";
		    }
		}
		else {
		    die "ERROR: expected multimapped XA targets of the form $base_genome" . "_chr_startpos_endpos_+.\nEncountered $fields[2]. Please re-create your reduced genome correctly and map again.\n$_";
		}
	    }
	    $full_line =~ s/(XA:\S+)/$new_XA_str/;
	}
	print $full_line;
    }
}

exit 0;
