#!/usr/bin/perl -w
use strict;
use Getopt::Long;

my $in; 
my $out;
my $log;
my %options = (
    "input=s"  => \$in,
    "output=s" => \$out,
    "log=s"    => \$log
    );

GetOptions(%options);

my $path = $0;
$path =~ s/\/\w*\.pl$//g;
system("perl $path/remap_reduced_SAM_hg19.pl -log $log < $in > $out ");

exit(0);
