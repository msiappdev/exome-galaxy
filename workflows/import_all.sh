#!/bin/bash

current_directory=$(readlink -f `dirname $0`)
api_directory="$current_directory/../scripts/api"
api_key=$1
shift

function import_workflow() {
    file=$1
    path="$current_directory/$file"
    if [[ ! -e "$file.$api_key.imported" ]];
    then
        echo "Importing workflow $path"
        /bin/bash -c "cd $api_directory; python workflow_import.py $api_key \"http://localhost\" \"$path\" --add_to_menu"
        touch "$file.$api_key.imported"
    fi
}

if [ ! $# == "0" ];
then
    for file in $*;
    do
        import_workflow "$file"
    done
else
    for file in *.ga;
    do
        import_workflow "$file"
    done
fi
